/////////////////////////////////////////////////////////////////////
// TMGBC 東京都スポーツ施設サービス 入力支援ツール
// TMGBC ver. 0.9.1
// author webmaster@infostc.org 2014/08/01
/////////////////////////////////////////////////////////////////////

var LOGIN_PASSWD = "88888888";

var POLLING_INTERVAL = 900000; // 15min * 60sec * 1000msec = 900000

// global params
var remote_window;
var surface;
var court;
var court_name;
var date;
var month;
var day;
var star;
var end;
var koma;
var card_id;
var card_ids;
var disp_num;
var card_index;
var action_code;
var ACTION_CODE_ENTRY = 1;
var ACTION_CODE_CHECK = 2;
var ACTION_CODE_ENTRYCHECK = 3;
var ACTION_CODE_POLLING = 4;
var message_buf = "";
var retry = 0;

document.OnLoad = start();

/////////////////////////////////////////////////////////////////////
// ページ読込時の処理
/////////////////////////////////////////////////////////////////////
function start() {

  var pathname = location.pathname;
  var titlename = document.title;
  if(pathname=="/"){
    document.open("web/pawae1001.jsp","menu_window", 'width=400, height=400, menubar=no, toolbar=no, scrollbars=yes');
  }else if(pathname=="/web/pawae1001.jsp"){
      $('html:first').html(menu_entry_html);
      action_code = ACTION_CODE_ENTRY;
      $(document).on('click', '#menu_entry', function(){
        menu_entry();
      });
      $(document).on('click', '#menu_entrycheck', function(){
        menu_entrycheck();
      });
      $(document).on('click', '#menu_check', function(){
        menu_check();
      });
      $(document).on('click', '#menu_polling', function(){
        menu_polling();
      });
      $(document).on('click', '#entry', function(){
        entry();
      });
      $(document).on('click', '#entrycheck', function(){
        entrycheck();
      });
      $(document).on('click', '#check', function(){
        check();
      });
      $(document).on('click', '#polling', function(){
        polling();
      });
      $(document).on('click', '#status', function(){
        status();
      });
  }else{
    sleep(1000, function (){ window.opener.doRemoteAction(titlename); } );
  }
}

function menu_entry(){
  $('html:first').html(menu_entry_html);
  action_code = ACTION_CODE_ENTRY;
}

function menu_entrycheck(){
  $('html:first').html(menu_entrycheck_html);
  action_code = ACTION_CODE_ENTRYCHECK;
}

function menu_check(){
  $('html:first').html(menu_check_html);
  action_code = ACTION_CODE_CHECK;
}

function menu_polling(){
  $('html:first').html(menu_polling_html);
  action_code = ACTION_CODE_POLLING;
}

function entry(){
  get_params();
  card_index=0;
  message_buf = "";
  startRemoteAction();
}

function entrycheck(){
  get_params();
  card_index=0;
  message_buf = "";
  startRemoteAction();
}

function check(){
  get_params();
  card_index=0;
  message_buf = "";
  startRemoteAction();
}

function polling(){
  get_params();
  card_index=0;
  message_buf = "";
  startRemotePollingAction();
}

function status(){
  if(message_buf == ""){
    alert("実行待ち");
  }else{
    alert(message_buf);
  }
}

function startRemoteAction(){
  if(card_index < card_ids.length){
    card_id = card_ids[card_index];
    if(card_id){
      print(card_id);
      card_index++;
      retry = 0;
      open_window();
    }else{
      card_index++;
      startRemoteAction();
    }
  }else{
    success("終了しました！");
  }
}

function startRemotePollingAction(){
  card_id = card_ids[0];
  if(card_id){
    retry = 0;
    open_window();
  }
}

function open_window(){
  disp_num = 1;
  remote_window = window.open("https://yoyaku.sports.metro.tokyo.jp/web/index.jsp", "remote_window", "scrollbars=yes, width=1200, height=600");
  sleep(1000, function (){ remote_window = window.open("https://yoyaku.sports.metro.tokyo.jp/web/pawae1002.jsp", "remote_window"); } );
}

function doRemoteAction(titlename){
  switch ( titlename ){
    case '':
      if(disp_num==1){
        a_link_click(["gRsvLoginUserAction"]);
      }else{
        error("[E0100] システムエラーです。");
      }
      break;
    case 'TMGBC 認証画面':
      if(disp_num==2){
        remote_window.document.getElementsByName('userId')[0].value=card_id;
        remote_window.document.getElementsByName('password')[0].value=LOGIN_PASSWD;
        sleep(3000, function(){
          a_link_click(["submitLogin"]);
        })
      }else{
        error("[E0200] システムエラーです。");
      }
      break;
    case 'TMGBC 登録メニュー画面':
      if(action_code == ACTION_CODE_ENTRY && (disp_num==3 || disp_num==12)){
        a_link_click(["gLotWSetupLotAcceptAction"]);
      }else if(action_code == ACTION_CODE_ENTRYCHECK && disp_num==3){
        a_link_click(["gLotWTransCompleteLotListAction"]);
      }else if(action_code == ACTION_CODE_CHECK && disp_num==3){
        a_link_click(["gLotWTransLotElectListAction"]);
      }else if(action_code == ACTION_CODE_POLLING && disp_num==3){
        a_link_click(["formWTransInstSrchVacantAction"]);
      }else{
        error("[E0300] システムエラーです。");
      }
      break;
    case 'TMGBC 抽選申込画面':
      if(disp_num==4 || disp_num==4){
        a_link_click(["lotWTransLotAcceptListAction"]);
      }else{
        error("[E0900] システムエラーです。");
      }
      break;
    case 'TMGBC 抽選種目一覧画面':
      if(disp_num==5 || disp_num==13){
        a_link_click(["lotWTransLotBldGrpAction",surface]);
      }else{
        error("[E0400] システムエラーです。");
      }
      break;
    case 'TMGBC 公園選択画面':
      if(disp_num==6 || disp_num==14){
        a_link_click(["lotWTransLotInstGrpAction",surface,court]);
      }else{
        error("[E0500] システムエラーです。");
      }
      break;
    case 'TMGBC 利用日時設定画面':
      if(disp_num>=7 || disp_num==15){
        if(a_link_click(["gLotWTransLotInstSrchVacantPageMoveAction",date,start,end])){
          disp_num = 6;
        }else{
          var dnum = (disp_num-6)*7+1;
          var d = ("0"+dnum).slice(-2);
          var y = year + month + d;
          a_link_click(["gLotWTransLotInstSrchVacantAction",y]);
        }
      }else if(disp_num==6 || disp_num==16){
        disp_num = 8;
        a_link_click(["gLotWInstTempLotApplyAction"]);
      }else{
        error("[E0600] システムエラーです。");
        error("[E0601] 日時指定を確認してください。");
      }
      break;
    case 'TMGBC 申込内容確認画面':
      if(disp_num==9 || disp_num==18){
        a_link_click(["gLotWInstLotApplyAction"]);
      }else if(disp_num==10){
        print(" Skip\n");
        sleep(500, function (){ startRemoteAction(); } );
      }else{
        error("[E0900] システムエラーです。");
      }
      break;
    case 'TMGBC 抽選申込完了確認画面':
      if(disp_num==10 || disp_num==19){
        a_link_click(["gLotWInstLotApplyMailNotAction"]);
      }else{
        error("[E1000] システムエラーです。");
      }
      break;
    case 'TMGBC 抽選完了画面':
      if(disp_num==11){
        a_link_click(["gRsvWTransUserAttestationEndAction"]);
        sleep(500, function (){ startRemoteAction(); } );
        print(" OK\n");
      }else if(disp_num==20){
        remote_window.doAction( remote_window.document.forms.formdisp, gRsvWTransUserAttestationEndAction);
        disp_num++;
        print(" OK\n");
        sleep(2000, function (){ startRemoteAction(); } );
      }else{
        error("[E1100] システムエラーです。");
      }
      break;
    case 'TMGBC 東京都スポーツ施設サービス':
      // 終了待ち
      break;
    case 'TMGBC 予約申込画面':
      if(action_code == ACTION_CODE_POLLING && disp_num==4){
        a_link_click(["gRsvWTransInstSrchMultipleAction"]);
      }else{
        error("[E0400] システムエラーです。");
      }
      break;
    case 'TMGBC 複合検索条件画面':
      if(action_code == ACTION_CODE_POLLING && disp_num==5){
        a_link_click(["gRsvWTransInstSrchPpsAction"]);
      }else if(action_code == ACTION_CODE_POLLING && disp_num==7){
        a_link_click([court]);
        var now = new Date();
        a_link_click(["changeMonthGif("+(Number(month) - now.getMonth() - 1)+")"]);
        a_link_click(["gRsvWGetInstSrchInfAction"]);
      }else{
        error("[E0500] システムエラーです。");
      }
      break;
    case 'TMGBC 種目検索条件画面':
      if(action_code == ACTION_CODE_POLLING && disp_num==6){
        a_link_click([surface]);
      }else{
        error("[E0600] システムエラーです。");
      }
      break;
    case 'TMGBC 施設空き状況画面時間帯貸し':
      if(action_code == ACTION_CODE_POLLING && disp_num==10){
        var a_link_flag = false;
          a_link = remote_window.document.getElementsByTagName("a");
          for(var i=0;i<a_link.length;i++){
            if(a_link[i].getAttribute('href')){
              if(a_link[i].getAttribute('href').indexOf("doInstSrchVacantSelectionAction")>0 && a_link[i].getAttribute('href').indexOf("0_"+(day - 1)+"_"+koma)>0){
                a_link[i].click();
                a_link_flag = true;
                disp_num++;
                break;
              }
            }
          }
          if(a_link_flag==false){
            // 空きなし
            var now = new Date();
            message_buf = "Last Date:\n"+now.getFullYear()+"/"+(now.getMonth()+1)+"/"+now.getDate()+" "+now.getHours()+":"+now.getMinutes();
            a_link_click(["gRsvWTransUserAttestationEndAction"]);
            sleep(POLLING_INTERVAL, function (){ startRemotePollingAction(); } );
          }
      }else{
        error("[E0600] システムエラーです。");
      }
      break;
    case 'TMGBC 予約内容一覧画面':
      if(action_code == ACTION_CODE_POLLING && disp_num==11){
        a_link_click(["gRsvWebReserveInstApplyAction"]);
      }else{
        error("[E0600] システムエラーです。");
      }
      break;
    case 'TMGBC 施設予約一覧画面':
      if(action_code == ACTION_CODE_POLLING && disp_num==12){
        a_link_click(["gRsvWTransUserAttestationEndAction"]);
        var now = new Date();
        message_buf = "Last Date:\n"+now.getFullYear()+"/"+(now.getMonth()+1)+"/"+now.getDate()+" "+now.getHours()+":"+now.getMinutes();
        success("確保！");
      }else{
        error("[E0600] システムエラーです。");
      }
      break;
    case 'TMGBC 抽選受付一覧画面':
      if(action_code == ACTION_CODE_ENTRYCHECK && disp_num==4){
        var get_num = remote_window.$('html:first').html().split(court_name).length - 1;
        message_buf += ": " + get_num + "\n";
        a_link_click(["gRsvWTransUserAttestationEndAction"]);
        sleep(0, function (){ startRemoteAction(); } );
      }else{
        error("[E0400] システムエラーです。"+disp_num);
      }
      break;
    case 'TMGBC 抽選結果確認画面':
      if(action_code == ACTION_CODE_CHECK && disp_num==4){
        var get_num = remote_window.$('html:first').html().split("selectElect").length - 1;
        if(get_num>0){
          message_buf += " ○(" + get_num + ")\n";
        }else{
          message_buf += " ×\n";
        }
        a_link_click(["gRsvWTransUserAttestationEndAction"]);
        sleep(0, function (){ startRemoteAction(); } );
      }else{
        error("[E0400] システムエラーです。"+disp_num);
      }
      break;
    case 'TMGBC お知らせ画面':
      a_link_click(["gRsvWUserMessageAction"]);
      --disp_num;
      break;
    case 'TMGBC':
      if(retry < 2){
        retry++;
        sleep(2000, function (){ open_window(); } );
      }else{
        error("[E0001] 一度ブラウザを閉じてから再度お試しください。");
      }
      break;
    default:
      error("[E0000] システムエラーです。["+titlename+"]");
      break;
  }
}

function get_params(){
  surface = $('#select_court').val() ? $('#select_court').val().split(':')[0] : 0;
  court   = $('#select_court').val() ? $('#select_court').val().split(':')[1] : 0;
  year    = $('#year').val();
  month   = $('#month').val();
  day     = $('#day').val();
  date   = year + month + day;
  start   = $('#start').val();
  end     = $('#end').val();
  koma    = $('#koma').val();
  card_ids= $('#card').val() ? $('#card').val().split('\n') : "";
  var selected_court = $('option[value="' + $('#select_court').val() + '"]')[0];
  if(selected_court) {
    var selected_court_text = selected_court.innerText;
    court_name = selected_court_text.split(' ')[1];
  }
}

function sleep(time, callback){
  setTimeout(callback, time);
}

function a_link_click(targets){
  a_link = remote_window.document.getElementsByTagName("a");
  for(var i=0;i<a_link.length;i++){
    if(a_link[i].getAttribute('href')){
      var a_link_flag_count = 0;
      for(var j=0;j<targets.length;j++){
        if(a_link[i].getAttribute('href').indexOf(targets[j])>0){
          a_link_flag_count++;
        }
      }
      if(a_link_flag_count == targets.length){
        a_link[i].click();
        disp_num++;
        return true;
      }
    }
  }
  return false;
}

function print(message){
  message_buf += message;
  $('#card').val(message_buf);
}

function success(message){
  if(action_code){
    $('#card').val(message_buf);
    message_buf = message + "\n--------------\n" + message_buf;
    alert(message_buf);
  }
}

function error(message){
  if(action_code){
    message_buf = message + "\n--------------\n" + message_buf;
    alert(message_buf);
    script_kill();
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Release note
// 0.9:トライアル
// 0.9.1:当選確認BugFix
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
