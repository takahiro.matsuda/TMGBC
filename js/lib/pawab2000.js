
function submitLogin(obj, action) {
	if (obj.userId.value == "") {
		showAlert(obj.e410000.value);
		obj.userId.focus();
		return;
	}
	if (obj.password.value == "") {
		showAlert(obj.e410010.value);
		obj.password.focus();
		return;
	}
	if (chkHanNum(obj.userId.value) == false) {
		showAlert(obj.e410320.value);
		resetUserID(obj);
		obj.userId.focus();
		return;
	}
	if (chkHanNum(obj.password.value) == false) {
		showAlert(obj.e410330.value);
		resetPassword(obj);
		obj.password.focus();
		return;
	}
 	doAction(obj, action)
}

var		inputName = new Array('id', 'PASSWORD');
var		tenkeyMax = 15;

function	tenkeyPush(obj,num)	{
	
	var	name = 0;
	var	text;
	var	id = 0;

	if (obj.fcflg.value==1){
		name=1;
		id = 1;
	}

	text = obj.elements[name].value;
	if (text.length >= tenkeyMax)	{
		return;
	}
	obj.elements[name].value = text + num;

}

function	prevkey(obj)	{	
	var	name = 0;
	var	text;
	var	tlength;
	if (obj.fcflg.value==1){
		name=1;
	}
	text = obj.elements[name].value;
	tlength = text.length;
	if (tlength > 0){
		obj.elements[name].value = text.substr(0,tlength-1);
	}
}
function	resetform(obj)	{
	obj.reset();

}
function	pw(obj)	{
	showAlert(obj);
	obj.fcflg.value="1";
}
function	fid(obj)	{
	obj.fcflg.value="0";
}
function	nextfocus(obj)	{
	var		text;
	var		tlength;
	if (obj.fcflg.value==1){
		obj.fcflg.value=0;
		obj.elements[0].focus();
	} else {
		name=inputName[1];
		obj.fcflg.value=1;
		obj.elements[1].focus();	
	}

}

function sendVacantFg(obj, action, vacantFg) {
		
	obj.vacantFg.value = vacantFg;

 	doAction(obj, action)
}

function mm_openbrwindow(theurl,winname,features) {
  window.open(theurl,winname,features);
}

function doReset(obj) {

	resetUserID(obj);
	resetPassword(obj);

}

function resetUserID(obj) {

	obj.userId.value = "";

}

function resetPassword(obj) {

	obj.password.value = "";

}

