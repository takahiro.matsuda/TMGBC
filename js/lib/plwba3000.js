
function sendLotApplyStop(obj, action, maxFieldCnt) {

	obj.selectUseYMD.value = "0";
	obj.selectTimeNum.value = "0";

	obj.selectDispStime.value = "-1";
	obj.selectDispEtime.value = "-1";


	if (maxFieldCnt != 1) {
		obj.selectFieldCnt.value = "";
	}

	for (var cnt = 0; cnt < gKomaNum; cnt++) {
		obj.selectKomaNo[cnt].value = "0";
		obj.selectStime[cnt].value = "-1";
		obj.selectEtime[cnt].value = "-1";
		obj.selectField[cnt].value = "0";
	}

 	doAction(obj, action);
}
function movePage(obj, action, srchStartYMD) {

	obj.srchStartYMD.value = srchStartYMD;

	setSelectParam(obj);

 	doAction(obj, action);
}
function doHelpAction(obj, action) {

	setSelectParam(obj);

 	doAction(obj, action);
}
function sendLotApplyCheck(obj, action) {

	var bSendFg = true;

	if (!(chkHissuField(obj.selectUseYMD, obj.e410170.value))) {
		bSendFg = false;

	} else if (obj.selectUseYMD.value == 0){

		showAlert(obj.e410170.value);
		bSendFg = false;

	} else {

		if (!(chkHissuField(obj.selectFieldCnt, obj.e410180.value))) {
			bSendFg = false;

		} else if (!(checkText(obj.selectFieldCnt, 0, obj.e410180.value, 7))) {
			bSendFg = false;

		} else if ((parseInt(obj.maxFieldCnt.value) > 0)
				&& (Number(obj.selectFieldCnt.value) > parseInt(obj.maxFieldCnt.value))) {

			showAlert(obj.e410210.value);
			obj.selectFieldCnt.focus();
			obj.selectFieldCnt.select();
			bSendFg = false;

		} else if (Number(obj.selectFieldCnt.value) > parseInt(gSelectInstVacantNum)) {

			showAlert(obj.e410210.value);
			obj.selectFieldCnt.focus();
			obj.selectFieldCnt.select();
			bSendFg = false;

		}
	}

	if (bSendFg) {
		obj.selectFieldCnt.value = Number(obj.selectFieldCnt.value);

		setSelectParam(obj);
 		doAction(obj, action);
	}
}

function selectOnKoma(obj, action, useYMD, stime, etime, instVacantNum, komaNo) {

	var komaNoArray = new Array();
	var stimeArray = new Array();
	var etimeArray = new Array();
	var fieldArray = new Array();



















	komaNoArray[0] = komaNo;
	stimeArray[0] = stime;
	etimeArray[0] = etime;
	fieldArray[0] = instVacantNum;

	fieldArray = sortArry2(komaNoArray, fieldArray);
	komaNoArray = sortArry(komaNoArray);
	stimeArray = sortArry(stimeArray);
	etimeArray = sortArry(etimeArray);

	obj.selectUseYMD.value = useYMD;
	obj.selectTimeNum.value = komaNoArray.length;

	if (stimeArray.length > 0) {
		obj.selectDispStime.value = stimeArray[0];
	} else {
		obj.selectDispStime.value = "-1";
	}

	if (etimeArray.length > 0) {
		obj.selectDispEtime.value = etimeArray[etimeArray.length - 1];
	} else {
		obj.selectDispEtime.value = "-1";
	}


	for (var cnt = 0; cnt < komaNoArray.length; cnt++) {
		obj.selectKomaNo[cnt].value = komaNoArray[cnt];
		obj.selectStime[cnt].value = stimeArray[cnt];
		obj.selectEtime[cnt].value = etimeArray[cnt];
		obj.selectField[cnt].value = fieldArray[cnt];
	}

	doAction(obj, action);
}

function isCheckSelectOn(obj, komaNo) {

	var beforeKomaNo = komaNo - 1;
	var afterKomaNo = komaNo + 1;

	var selectTimeNum = obj.selectTimeNum.value;

	for (var cnt = 0; cnt < selectTimeNum; cnt++) {
		if ((gSelectKomaNos[cnt] == beforeKomaNo)
				|| (gSelectKomaNos[cnt] == afterKomaNo)) {

			return true;
		}
	}

	return false;

}

function selectOffKoma(obj, action, useYMD, stime, etime, komaNo) {

	var komaNoArray = new Array();
	var stimeArray = new Array();
	var etimeArray = new Array();
	var fieldArray = new Array();

	if (isCheckSelectOff(obj, komaNo)) {

		var selectCnt = 0;

		for (var cnt = 0; cnt < obj.selectTimeNum.value; cnt++) {
			if (komaNo != gSelectKomaNos[cnt]) {
				komaNoArray[selectCnt] = gSelectKomaNos[cnt];
				stimeArray[selectCnt] = gSelectStimes[cnt];
				etimeArray[selectCnt] = gSelectEtimes[cnt];
				fieldArray[selectCnt] = gSelectFields[cnt];
				selectCnt++;
			}
		}

	} else {

		return;

	}

	if (komaNoArray.length > 0) {

		fieldArray = sortArry2(komaNoArray, fieldArray);
		komaNoArray = sortArry(komaNoArray);
		stimeArray = sortArry(stimeArray);
		etimeArray = sortArry(etimeArray);

		obj.selectUseYMD.value = useYMD;
		obj.selectTimeNum.value = komaNoArray.length;

		for (var cnt = 0; cnt < komaNoArray.length; cnt++) {
			obj.selectKomaNo[cnt].value = komaNoArray[cnt];
			obj.selectStime[cnt].value = stimeArray[cnt];
			obj.selectEtime[cnt].value = etimeArray[cnt];
			obj.selectField[cnt].value = fieldArray[cnt];
		}

		if (stimeArray.length > 0) {
			obj.selectDispStime.value = stimeArray[0];
		} else {
			obj.selectDispStime.value = "-1";
		}

		if (etimeArray.length > 0) {
			obj.selectDispEtime.value = etimeArray[etimeArray.length - 1];
		} else {
			obj.selectDispEtime.value = "-1";
		}
	} else {

		obj.selectUseYMD.value = "0";
		obj.selectTimeNum.value = "0";

		obj.selectDispStime.value = "-1";
		obj.selectDispEtime.value = "-1";
		obj.selectField.value = "0";
	}

	doAction(obj, action);
}

function isCheckSelectOff(obj, komaNo) {

	var bCheckFlg = true;

	var selectTimeNum = parseInt(obj.selectTimeNum.value);

	if (selectTimeNum >= 3) {

		var selectedFirstNo = parseInt(gSelectKomaNos[0]);
		var selectedLastNo = parseInt(gSelectKomaNos[selectTimeNum - 1]);

		if ((komaNo != selectedFirstNo) && (komaNo != selectedLastNo)) {
			bCheckFlg = false;
		}
	}

	return bCheckFlg;
}

function setSelectParam(obj) {

	for (var cnt = 0; cnt < obj.selectTimeNum.value; cnt++) {
		obj.selectKomaNo[cnt].value = gSelectKomaNos[cnt];
		obj.selectStime[cnt].value = gSelectStimes[cnt];
		obj.selectEtime[cnt].value = gSelectEtimes[cnt];
		obj.selectField[cnt].value = gSelectFields[cnt];
	}

}
function tenkeyPush(obj, num) {

	var maxlength = 4;

	var text = "";
	text = obj.value;

	if (parseInt(text.length) >= parseInt(maxlength)) {
		return;
	}

	obj.value = text + num;

}

function prevkey(obj) {

	var	text = "";

	text = obj.value;

	if (text.length > 0){
		obj.value = text.substr(0, text.length - 1);
	}
}

function sortArry(arry) {

	for(var i = 0; i < arry.length -1; i++){

	 	for(var j = arry.length - 1; j > i; j--) {

			if (parseInt(arry[j])  < parseInt(arry[j-1])) {

				var wkValue;

				wkValue = arry[j];

				arry[j] = arry[j - 1];

				arry[j- 1] = wkValue;

			}
		}
	}

	return arry;
}

function sortArry2(baseArry, arry) {

	var wkArray = new Array();
	for (var cnt = 0; cnt < baseArry.length; cnt++) {
		wkArray[cnt] = baseArry[cnt];
	}

	for(var i = 0; i < wkArray.length -1; i++){

	 	for(var j = wkArray.length - 1 ; j > i; j--) {
			if (parseInt(wkArray[j])  < parseInt(wkArray[j-1])) {
				var wkValue;
				wkValue = arry[j];
				arry[j] = arry[j - 1];
				arry[j- 1] = wkValue;

				var wkSortValue;
				wkSortValue = wkArray[j];
				wkArray[j] = wkArray[j - 1];
				wkArray[j- 1] = wkSortValue;
			}
		}
	}

	return arry;
}

