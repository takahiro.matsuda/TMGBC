var menu_entry_html ='\
<html>\
<head><title>TMGBC 東京都スポーツ施設サービス 入力支援ツール</title></head>\
<p><B>東京都スポーツ施設サービス 入力支援ツール</B></p>\
<hr />\
<input type="button" id="menu_entry" value="抽選申込" disabled="desabled" style="background-color: #80FF80;" />\
<input type="button" id="menu_entrycheck" value="抽選確認" />\
<input type="button" id="menu_check" value="当選確認" />\
<input type="button" id="menu_polling" value="キャンセル待ち" />\
<hr />\
<p>コート：<br />\
<select id="select_court">\
<option value="120:1310">(ハード) 大井ふ頭中央海浜公園Ａ</option>\
<option value="120:1315">(ハード) 大井ふ頭中央海浜公園Ｂ</option>\
<option value="120:2109">(ハード) 城北中央公園</option>\
<option value="120:1320">(ハード) 有明テニスの森公園Ａ</option>\
<option value="120:2126">(ハード) 有明テニスの森公園Ｂ</option>\
<option value="130:2328">(人工芝) 有明テニスの森公園Ｃ</option>\
<option value="130:2301">(人工芝) 日比谷公園</option>\
<option value="130:2302">(人工芝) 芝公園</option>\
<option value="130:2303">(人工芝) 猿江恩賜公園</option>\
<option value="130:2304">(人工芝) 亀戸中央公園</option>\
<option value="130:1060">(人工芝) 木場公園</option>\
<option value="130:2306">(人工芝) 祖師谷公園</option>\
<option value="130:2307">(人工芝) 東白鬚公園</option>\
<option value="130:2308">(人工芝) 浮間公園</option>\
<option value="130:2309">(人工芝) 城北中央公園</option>\
<option value="130:2310">(人工芝) 赤塚公園</option>\
<option value="130:2311">(人工芝) 東綾瀬公園</option>\
<option value="130:2312">(人工芝) 舎人公園</option>\
<option value="130:2313">(人工芝) 篠崎公園Ａ</option>\
<option value="130:2314">(人工芝) 大島小松川公園Ａ</option>\
<option value="130:2315">(人工芝) 善福寺川緑地</option>\
<option value="130:2316">(人工芝) 光が丘公園</option>\
<option value="130:2317">(人工芝) 井の頭恩賜公園</option>\
<option value="130:2318">(人工芝) 武蔵野中央公園</option>\
<option value="130:2319">(人工芝) 小金井公園</option>\
<option value="130:2320">(人工芝) 野川公園</option>\
<option value="130:2321">(人工芝) 府中の森公園</option>\
<option value="130:2323">(人工芝) 大井ふ頭海浜公園</option>\
<option value="130:2324">(人工芝) 有明テニスの森公園</option>\
<option value="130:2325">(人工芝) 東大和南公園</option>\
<option value="130:2326">(人工芝) 汐入公園</option>\
<option value="130:2327">(人工芝) 石神井公園Ｂ</option>\
</select>\
</p>\
<p>予約日時：<br />\
<select id="year">\
<option value="2016">2016年</option>\
<option value="2017">2017年</option>\
<option value="2018">2018年</option>\
<option value="2019">2019年</option>\
<option value="2020">2020年</option>\
</select>\
<select id="month">\
<option value="01">01月</option>\
<option value="02">02月</option>\
<option value="03">03月</option>\
<option value="04">04月</option>\
<option value="05">05月</option>\
<option value="06">06月</option>\
<option value="07">07月</option>\
<option value="08">08月</option>\
<option value="09">09月</option>\
<option value="10">10月</option>\
<option value="11">11月</option>\
<option value="12">12月</option>\
</select>\
<select id="day">\
<option value="01">01日</option>\
<option value="02">02日</option>\
<option value="03">03日</option>\
<option value="04">04日</option>\
<option value="05">05日</option>\
<option value="06">06日</option>\
<option value="07">07日</option>\
<option value="08">08日</option>\
<option value="09">09日</option>\
<option value="10">10日</option>\
<option value="11">11日</option>\
<option value="12">12日</option>\
<option value="13">13日</option>\
<option value="14">14日</option>\
<option value="15">15日</option>\
<option value="16">16日</option>\
<option value="17">17日</option>\
<option value="18">18日</option>\
<option value="19">19日</option>\
<option value="20">20日</option>\
<option value="21">21日</option>\
<option value="22">22日</option>\
<option value="23">23日</option>\
<option value="24">24日</option>\
<option value="25">25日</option>\
<option value="26">26日</option>\
<option value="27">27日</option>\
<option value="28">28日</option>\
<option value="29">29日</option>\
<option value="30">30日</option>\
<option value="31">31日</option>\
</select>\
 \
<select id="start">\
<option value="800">08:00</option>\
<option value="900">09:00</option>\
<option value="1000">10:00</option>\
<option value="1100">11:00</option>\
<option value="1200">12:00</option>\
<option value="1300">13:00</option>\
<option value="1400">14:00</option>\
<option value="1500">15:00</option>\
<option value="1600">16:00</option>\
<option value="1700">17:00</option>\
<option value="1800">18:00</option>\
<option value="1900">19:00</option>\
<option value="2000">20:00</option>\
<option value="2100">21:00</option>\
<option value="2200">22:00</option>\
</select>\
-\
<select id="end">\
<option value="800">08:00</option>\
<option value="900">09:00</option>\
<option value="1000">10:00</option>\
<option value="1100">11:00</option>\
<option value="1200">12:00</option>\
<option value="1300">13:00</option>\
<option value="1400">14:00</option>\
<option value="1500">15:00</option>\
<option value="1600">16:00</option>\
<option value="1700">17:00</option>\
<option value="1800">18:00</option>\
<option value="1900">19:00</option>\
<option value="2000">20:00</option>\
<option value="2100">21:00</option>\
<option value="2200">22:00</option>\
</select>\
</p>\
<p>コートカード：<br />\
<textarea id="card"></textarea>\
</p><br />\
<input type="button" id="entry" value="抽選申込" />\
<input type="button" id="status" value="ステータス確認" />\
';

var menu_entrycheck_html ='\
<html>\
<head><title>TMGBC 東京都スポーツ施設サービス 入力支援ツール</title></head>\
<p><B>東京都スポーツ施設サービス 入力支援ツール</B></p>\
<hr />\
<input type="button" id="menu_entry" value="抽選申込" />\
<input type="button" id="menu_entrycheck" value="抽選確認" disabled="desabled" style="background-color: #80FF80;"/>\
<input type="button" id="menu_check" value="当選確認" />\
<input type="button" id="menu_polling" value="キャンセル待ち" />\
<hr />\
<p>コート：<br />\
<select id="select_court">\
<option value="120:1310">(ハード) 大井ふ頭中央海浜公園Ａ</option>\
<option value="120:1315">(ハード) 大井ふ頭中央海浜公園Ｂ</option>\
<option value="120:2109">(ハード) 城北中央公園</option>\
<option value="120:1320">(ハード) 有明テニスの森公園Ａ</option>\
<option value="120:2126">(ハード) 有明テニスの森公園Ｂ</option>\
<option value="130:2328">(人工芝) 有明テニスの森公園Ｃ</option>\
<option value="130:2301">(人工芝) 日比谷公園</option>\
<option value="130:2302">(人工芝) 芝公園</option>\
<option value="130:2303">(人工芝) 猿江恩賜公園</option>\
<option value="130:2304">(人工芝) 亀戸中央公園</option>\
<option value="130:2305">(人工芝) 木場公園</option>\
<option value="130:2306">(人工芝) 祖師谷公園</option>\
<option value="130:2307">(人工芝) 東白鬚公園</option>\
<option value="130:2308">(人工芝) 浮間公園</option>\
<option value="130:2309">(人工芝) 城北中央公園</option>\
<option value="130:2310">(人工芝) 赤塚公園</option>\
<option value="130:2311">(人工芝) 東綾瀬公園</option>\
<option value="130:2312">(人工芝) 舎人公園</option>\
<option value="130:2313">(人工芝) 篠崎公園Ａ</option>\
<option value="130:2314">(人工芝) 大島小松川公園Ａ</option>\
<option value="130:2315">(人工芝) 善福寺川緑地</option>\
<option value="130:2316">(人工芝) 光が丘公園</option>\
<option value="130:2317">(人工芝) 井の頭恩賜公園</option>\
<option value="130:2318">(人工芝) 武蔵野中央公園</option>\
<option value="130:2319">(人工芝) 小金井公園</option>\
<option value="130:2320">(人工芝) 野川公園</option>\
<option value="130:2321">(人工芝) 府中の森公園</option>\
<option value="130:2323">(人工芝) 大井ふ頭海浜公園</option>\
<option value="130:2324">(人工芝) 有明テニスの森公園</option>\
<option value="130:2325">(人工芝) 東大和南公園</option>\
<option value="130:2326">(人工芝) 汐入公園</option>\
<option value="130:2327">(人工芝) 石神井公園Ｂ</option>\
</select>\
<p>コートカード：<br />\
<textarea id="card"></textarea>\
</p><br />\
<input type="button" id="entrycheck" value="抽選確認" />\
<input type="button" id="status" value="ステータス確認" />\
';

var menu_check_html ='\
<html>\
<head><title>TMGBC 東京都スポーツ施設サービス 入力支援ツール</title></head>\
<p><B>東京都スポーツ施設サービス 入力支援ツール</B></p>\
<hr />\
<input type="button" id="menu_entry" value="抽選申込" />\
<input type="button" id="menu_entrycheck" value="抽選確認" />\
<input type="button" id="menu_check" value="当選確認" disabled="desabled" style="background-color: #80FF80;" />\
<input type="button" id="menu_polling" value="キャンセル待ち" />\
<hr />\
<p>コートカード：<br />\
<textarea id="card"></textarea>\
</p><br />\
<input type="button" id="check" value="当選確認" />\
<input type="button" id="status" value="ステータス確認" />\
';

var menu_polling_html ='\
<html>\
<head><title>TMGBC 東京都スポーツ施設サービス 入力支援ツール</title></head>\
<p><B>東京都スポーツ施設サービス 入力支援ツール</B></p>\
<hr />\
<input type="button" id="menu_entry" value="抽選申込" />\
<input type="button" id="menu_entrycheck" value="抽選確認" />\
<input type="button" id="menu_check" value="当選確認" />\
<input type="button" id="menu_polling" value="キャンセル待ち" disabled="desabled" style="background-color: #80FF80;" />\
<hr />\
<p>コート：<br />\
<select id="select_court">\
<option value="120:0061">(ハード) 大井ふ頭海浜公園</option>\
<option value="120:0018">(ハード) 城北中央公園</option>\
<option value="120:0063">(ハード) 有明テニスの森公園Ａ</option>\
<option value="120:0064">(ハード) 有明テニスの森公園Ｂ</option>\
<option value="130:0065">(人工芝) 有明テニスの森公園Ｃ</option>\
<option value="130:0001">(人工芝) 日比谷公園</option>\
<option value="130:0002">(人工芝) 芝公園</option>\
<option value="130:0005">(人工芝) 猿江恩賜公園</option>\
<option value="130:0006">(人工芝) 亀戸中央公園</option>\
<option value="130:0008">(人工芝) 木場公園</option>\
<option value="130:0009">(人工芝) 祖師谷公園</option>\
<option value="130:0016">(人工芝) 東白鬚公園</option>\
<option value="130:0017">(人工芝) 浮間公園</option>\
<option value="130:0018">(人工芝) 城北中央公園</option>\
<option value="130:0019">(人工芝) 赤塚公園</option>\
<option value="130:0020">(人工芝) 東綾瀬公園</option>\
<option value="130:0021">(人工芝) 舎人公園</option>\
<option value="130:0022">(人工芝) 篠崎公園Ａ</option>\
<option value="130:0024">(人工芝) 大島小松川公園Ａ</option>\
<option value="130:0025">(人工芝) 汐入公園</option>\
<option value="130:0035">(人工芝) 善福寺川緑地</option>\
<option value="130:0036">(人工芝) 石神井公園Ｂ</option>\
<option value="130:0037">(人工芝) 光が丘公園</option>\
<option value="130:0040">(人工芝) 井の頭恩賜公園</option>\
<option value="130:0041">(人工芝) 武蔵野中央公園</option>\
<option value="130:0042">(人工芝) 小金井公園</option>\
<option value="130:0044">(人工芝) 野川公園</option>\
<option value="130:0045">(人工芝) 府中の森公園</option>\
<option value="130:0046">(人工芝) 東大和南公園</option>\
<option value="130:0061">(人工芝) 大井ふ頭海浜公園</option>\
</select>\
</p>\
<p>予約日時：<br />\
<select id="year">\
<option value="2016">2016年</option>\
<option value="2017">2017年</option>\
<option value="2018">2018年</option>\
<option value="2019">2019年</option>\
<option value="2020">2020年</option>\</select>\
<select id="month">\
<option value="01">01月</option>\
<option value="02">02月</option>\
<option value="03">03月</option>\
<option value="04">04月</option>\
<option value="05">05月</option>\
<option value="06">06月</option>\
<option value="07">07月</option>\
<option value="08">08月</option>\
<option value="09">09月</option>\
<option value="10">10月</option>\
<option value="11">11月</option>\
<option value="12">12月</option>\
</select>\
<select id="day">\
<option value="01">01日</option>\
<option value="02">02日</option>\
<option value="03">03日</option>\
<option value="04">04日</option>\
<option value="05">05日</option>\
<option value="06">06日</option>\
<option value="07">07日</option>\
<option value="08">08日</option>\
<option value="09">09日</option>\
<option value="10">10日</option>\
<option value="11">11日</option>\
<option value="12">12日</option>\
<option value="13">13日</option>\
<option value="14">14日</option>\
<option value="15">15日</option>\
<option value="16">16日</option>\
<option value="17">17日</option>\
<option value="18">18日</option>\
<option value="19">19日</option>\
<option value="20">20日</option>\
<option value="21">21日</option>\
<option value="22">22日</option>\
<option value="23">23日</option>\
<option value="24">24日</option>\
<option value="25">25日</option>\
<option value="26">26日</option>\
<option value="27">27日</option>\
<option value="28">28日</option>\
<option value="29">29日</option>\
<option value="30">30日</option>\
<option value="31">31日</option>\
</select>\
 \
<select id="koma">\
<option value="0">1コマ目</option>\
<option value="1">2コマ目</option>\
<option value="2">3コマ目</option>\
<option value="3">4コマ目</option>\
<option value="4">5コマ目</option>\
<option value="5">6コマ目</option>\
<option value="6">7コマ目</option>\
<option value="7">8コマ目</option>\
</select>\
</p>\
<p>コートカード：<br />\
<input type="text" id="card" />\
</p><br />\
<input type="button" id="polling" value="キャンセル待ち" />\
<input type="button" id="status" value="ステータス確認" />\
';

